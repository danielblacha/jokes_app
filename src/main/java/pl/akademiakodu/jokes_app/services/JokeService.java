package pl.akademiakodu.jokes_app.services;

/**
 * @author daniel on 2018-09-25
 */
public interface JokeService {

    String getJoke();
}
