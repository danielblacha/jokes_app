package pl.akademiakodu.jokes_app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.akademiakodu.jokes_app.services.JokeService;

/**
 * @author daniel on 2018-09-25
 */
@Controller
public class JokeController {

    @Autowired
    private JokeService jokeService;

    @RequestMapping({"/",""})
    public String showJoke(Model model){
        model.addAttribute("joke", jokeService.getJoke());

        return "chucknorris";
    }
}
